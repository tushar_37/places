class Place {
    constructor(id, name, city, state, information, image) {
        this.id = id
        this.name = name
        this.city = city
        this.state = state
        this.information = information
        this.image = image
    }
}

export default Place
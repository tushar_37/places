export const GET_DATA = "GET_DATA"

export const getData = url => {
    return {
        type: GET_DATA,
        imageUrl: url
    }
}
import Place from "../../../modal/Place"

export const ADD_PLACE = "ADD_PLACE"


export const fetchPlaces = () => {
    return async dispatch => {
        try {
            const response = await fetch('https://places-bd599-default-rtdb.firebaseio.com/NewPlaces.json')

            if (!response.ok) {
                throw new Error('Smething went wrong...')
            }
            const values = await response.json()
            for (const key in values) {
                const newPlace = new Place(key, values[key].name, values[key].city, values[key].state, values[key].information, values[key].image)
                dispatch({
                    type: ADD_PLACE,
                    place: newPlace
                })
            }
        } catch (exception) {
            console.log('EXCEPTION::::', exception)
            throw exception
        }

    }
}

export const addPlace = place => {
    return async dispatch => {
        const response = await fetch('https://places-bd599-default-rtdb.firebaseio.com/NewPlaces.json', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                name: place.name,
                city: place.city,
                state: place.state,
                information: place.information,
                image: place.image
            })
        })
        const values = await response.json()
        console.log('values from firebase ::::', values.name)
        place.id = values.name
        dispatch({
            type: ADD_PLACE,
            place: place
        })
    }

}

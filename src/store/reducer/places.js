import Places from '../../../data/PlacesData'
import { ADD_PLACE } from '../action/places'


const initialState = {
    places: Places
}

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_PLACE:
            return {
                ...state,
                places: [...state.places, action.place]
            }

        default:
            console.log('default case')
    }

    return state
}
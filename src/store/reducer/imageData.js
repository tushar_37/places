import { GET_DATA } from "../action/imageData"

const initialState = {
    imageUrl: ''
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_DATA:
            return {
                ...state,
                imageUrl: action.imageUrl
            }
    }
    return state
}
import Place from '../modal/Place'


const Places = [
    new Place(1,
        'Lonawala',
        'Pune',
        'Maharastra',
        'Lonavala is a hill station surrounded by green valleys in western India near Mumbai. The Karla Caves and the Bhaja Caves are ancient Buddhist shrines carved out of the rock. They feature massive pillars and intricate relief sculptures. South of the Bhaja Caves sits the imposing Lohagad Fort, with its 4 gates. West of here is Bhushi Dam, where water overflows onto a set of steps during rainy season.',
        'https://img.traveltriangle.com/blog/wp-content/uploads/2020/01/Rajmachi_20-Jan.jpg'),

    new Place(2,
        'Wankhede Stadium',
        'Mumbai',
        'Maharastra',
        'The Wankhede Stadium is an international cricket stadium in Mumbai, India. The stadium now has a capacity of 33,108, following renovations for the 2011 Cricket World Cup. Before the upgrade, the capacity was approximately 45,000',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjROQYY4A_ZHTUjy0gprkBkf-kFKSinzyvTw&usqp=CAU'),

    // new Place(3,
    //     'Qutab Minar',
    //     'New Delhi',
    //     'Delhi',
    //     'The Qutb Minar, also spelled as Qutub Minar and Qutab Minar, is a minaret and "victory tower" that forms part of the Qutb complex, a UNESCO World Heritage Site in the Mehrauli area of New Delhi, India. The height of Qutb Minar is 72.5 meters, making it the tallest minaret in the world built of bricks',
    //     'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSuvSVCX7fj4Gcb_Ni1bUBTFfIP3w7muDgaIg&usqp=CAU'),

    // new Place(4,
    //     'Hawa Mahal',
    //     'Jaipur',
    //     'Rajasthan',
    //     'Hawa Mahal is a palace in Jaipur, India approximately 300 kilometers from the capital city of Delhi. Built from red and pink sandstone, the palace sits on the edge of the City Palace, Jaipur, and extends to the Zenana, or women\'s chambers',
    //     'https://www.holidify.com/images/cmsuploads/compressed/h4_20170822181427.PNG')
]

export default Places
import React from 'react'
import { Dimensions, StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: Dimensions.get('window').width - 20,
        height: 300,
        borderRadius: 10
    },
    placeHeader: {
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    placeTittle: {
        fontSize: 24,
    },
    city: {
        fontSize: 18,
        color: 'grey'
    },
    detailView: {
        justifyContent: 'center',
        width: Dimensions.get('window').width - 20,
        padding: 20
    },
    infoText: {
        fontSize: 24, color: 'blue', marginLeft: 30, fontWeight: '500'
    },
    underline: {
        backgroundColor: 'blue', height: 1, width: 40, marginLeft: 30, margin: 10
    },
    information: {
        fontSize: 20,
        fontWeight: '300'
    }
})

export default styles
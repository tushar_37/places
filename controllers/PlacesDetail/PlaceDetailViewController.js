import React from 'react'
import { useEffect } from 'react'
import { View, Image, ScrollView, Text, TouchableOpacity, Dimensions } from "react-native"
import ImageZoom from 'react-native-image-pan-zoom'
import styles from './style'

const PlaceDetailViewController = props => {
    useEffect(() => {
        props.navigation.setOptions({
            title: props.route.params.name
        })
    })

    return (
        <ScrollView>
            <ImageZoom cropWidth={Dimensions.get('window').width}
                cropHeight={300}
                imageWidth={Dimensions.get('window').width - 20}
                imageHeight={300}>
                <Image
                    style={styles.image}
                    source={{ uri: props.route.params.image }} />
            </ImageZoom>

            <View style={styles.placeHeader}>
                <Text style={styles.placeTittle}>
                    {props.route.params.name}</Text>
                <Text style={styles.city}>{props.route.params.city} ,<Text>{props.route.params.state}</Text></Text>
            </View>

            <View style={styles.detailView}>
                <Text style={styles.infoText}>Info</Text>
                <Text style={styles.underline}>Info</Text>
                <Text style={styles.information}>{props.route.params.information}</Text>
            </View>

        </ScrollView>
    )
}

export default PlaceDetailViewController
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { useDispatch } from 'react-redux';
import * as ImageAction from '../../src/store/action/imageData'
import styles from './style'

const TakePictureViewControllr = props => {

    const dispatch = useDispatch()

    const takePicture = async () => {
        if (RNCamera.camera) {
            const options = { quality: 0.5 };
            const data = await RNCamera.camera.takePictureAsync(options);
            dispatch(ImageAction.getData(data.uri))
            props.navigation.goBack();
        }
    }

    return (
        <View style={styles.container}>
            <RNCamera
                ref={ref => {
                    RNCamera.camera = ref;
                }}
                style={styles.preview}
                type={RNCamera.Constants.Type.back}
            />
                <TouchableOpacity
                    onPress={takePicture}
                    style={styles.capture}>
                    <Text style={{ fontSize: 14 }}> SNAP </Text>
                </TouchableOpacity>
            </View>
    );



}

export default TakePictureViewControllr
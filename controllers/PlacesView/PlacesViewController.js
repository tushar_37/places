import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native'
import styles from './style'
import PlaceView from '../../components/PlaceView/PlaceView'
import { useSelector, useDispatch } from 'react-redux'
import * as PlacesActions from '../../src/store/action/places'
const PlacesViewController = props => {

    const [isLoading, setLoading] = useState(false)

    const dispatch = useDispatch()

    const Places = useSelector(state => state.places.places)

    const didSelectdetail = (placeDetails) => {
        props.navigation.navigate('Details', placeDetails)
    }

    const didSelectAddPlace = () => {
        props.navigation.navigate('New Place')
    }

    const renderBarRightBtn = () => {
        return (
            <TouchableOpacity onPress={didSelectAddPlace}>
                <Text style={{ ...styles.rightBarBtn, fontSize: 22, marginRight: 15 }}> + </Text>
            </TouchableOpacity>
        )
    }


    useEffect(() => {
        props.navigation.setOptions({
            headerRight: renderBarRightBtn
        })
        const fetchData = async () => {
            setLoading(true)
            try {
                await dispatch(PlacesActions.fetchPlaces())
            } catch (exception) {
                console.log('Exception in fetchPlaces:::', exception)
            }
            setLoading(false)
        }
        fetchData()

    }, [dispatch])

    if (isLoading) {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                <ActivityIndicator size='large' color='orange' />
            </View>

        )
    }
    return (
        <View style={styles.screen}>

            <FlatList
                data={Places}
                keyExtractor={item => item.id}
                renderItem={itemData => {
                    console.log(itemData)
                    return (
                        <PlaceView
                            name={itemData.item.name}
                            city={itemData.item.city}
                            state={itemData.item.state}
                            description={itemData.item.information}
                            image={itemData.item.image}
                            onPress={() => didSelectdetail(itemData.item)}
                        />
                    )
                }}
            />

        </View>
    )
}


export default PlacesViewController
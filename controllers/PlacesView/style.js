import { StyleSheet } from 'react-native'


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rightBarBtn: { 
        color: 'blue', 
        fontWeight: 'bold', 
        padding: 5, 
        margin: 5, 
        borderRadius: 5, 
        borderWidth: 1 
    }
})


export default styles
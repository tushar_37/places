import React, { useEffect, useState } from 'react'
import { View, Text, ScrollView, TextInput, TouchableOpacity } from 'react-native'
import ImagePickerView from '../../components/ImagePicker/ImagePickerView'
import styles from './style'
import * as AddPlaceAction from '../../src/store/action/places'
import * as ImageAction from '../../src/store/action/imageData'

import Place from '../../modal/Place'


import { useDispatch, useSelector } from 'react-redux'
const AddPlaceViewController = props => {

    const imageUrl = useSelector(state => state.image.imageUrl)

    const dispatch = useDispatch()
    const didSelectAddPlace = () => {
        const newPlace = new Place(0, name, city, state, description, imageUrl)
        dispatch(AddPlaceAction.addPlace(newPlace))
        dispatch(ImageAction.getData(''))

        props.navigation.goBack();
    }
    const [name, setName] = useState('')
    const [city, setCity] = useState('')
    const [state, setState] = useState('')
    const [description, setDescription] = useState('')

    const renderRightBarButton = () => {
        return (
            <TouchableOpacity onPress={didSelectAddPlace}>
                <Text style={styles.rightBarBtn}>SAVE</Text>
            </TouchableOpacity>
        )
    }

    useEffect(() => {
        props.navigation.setOptions({
            headerRight: renderRightBarButton
        })
    })

    return (
        <ScrollView>
            <View style={styles.screen}>

                <View style={styles.inputViewContainer}>
                    <TextInput
                        style={styles.inputField}
                        placeholder="place name, eg. Red Fort"
                        onChangeText={text => setName(text)} />
                </View>

                <View style={styles.inputViewContainer}>
                    <TextInput
                        style={styles.inputField}
                        placeholder="city of place, eg. Pune"
                        onChangeText={text => setCity(text)} />
                </View>

                <View style={styles.inputViewContainer}>
                    <TextInput
                        style={styles.inputField}
                        placeholder="city belongs to, eg. Maharastra"
                        onChangeText={text => setState(text)} />
                </View>

                <View style={styles.inputViewContainer}>
                    <TextInput
                        multiline
                        style={styles.inputField}
                        placeholder="place information, eg.great place"
                        onChangeText={text => setDescription(text)} />
                </View>


                <TouchableOpacity
                    onPress={() => props.navigation.navigate('Camera')}>
                    <Text style={styles.openCamera}>Open Camera</Text>
                </TouchableOpacity>

            </View>

            <ImagePickerView selectedImage={imageUrl} />

        </ScrollView>
    )
}

export default AddPlaceViewController
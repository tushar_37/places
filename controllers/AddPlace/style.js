import React from 'react'
import { Dimensions, StyleSheet } from 'react-native'


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rightBarBtn: { 
        color: 'blue', 
        fontWeight: 'bold', 
        padding: 5, 
        margin: 5, 
        borderRadius: 5, 
        borderWidth: 1 
    },
    inputViewContainer: {
        width: Dimensions.get('window').width - 20,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    inputField: {
        width: Dimensions.get('window').width - 40,
        padding: 10,
        borderRadius: 10,
        backgroundColor: '#fff'
    },
    imageConatiner: {
        margin: 10,
        borderRadius: 10,
        width: Dimensions.get('window').width - 20,
        height: 300
    },
    openCamera:{ color: 'blue', fontSize: 18, fontWeight: '900', padding: 10 }
})


export default styles
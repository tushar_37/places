import React from 'react'
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import PlacesViewController from '../controllers/PlacesView/PlacesViewController'
import PlaceDetailViewController from '../controllers/PlacesDetail/PlaceDetailViewController';
import AddPlaceViewController from '../controllers/AddPlace/AddPlaceViewController'

import PlaceReducer from '../src/store/reducer/places'
import ImageReducer from '../src/store/reducer/imageData'
import { combineReducers, createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import ReduxThunk from 'redux-thunk'
import TakePictureViewControllr from '../controllers/TakePicture/TakePictureViewController';

const rootReducer = combineReducers({
    places: PlaceReducer,
    image: ImageReducer
})

const store = createStore(rootReducer, applyMiddleware(ReduxThunk))

const Stack = createStackNavigator()
const PlacesNavigationFlow = props => {
    return (
        <Provider store={store}>
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name="Places" component={PlacesViewController} />
                    <Stack.Screen name="Details" component={PlaceDetailViewController} />
                    <Stack.Screen name="New Place" component={AddPlaceViewController} />
                    <Stack.Screen name="Camera" component={TakePictureViewControllr} />
                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    )
}


export default PlacesNavigationFlow
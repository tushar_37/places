import React, { useState } from 'react'
import { View, Image, Text, TouchableOpacity, Alert } from 'react-native'
import { RNCamera } from 'react-native-camera'
import styles from './style'


const ImagePickerView = ({selectedImage}) => {

    return (
        <View style={styles.ImagePickerSetup}>
            <View style={styles.imagePreview}>
                {!selectedImage ? <Text>Open Camera to add image</Text> :
                    <Image
                        style={styles.image}
                        source={{ uri: selectedImage }}
                    />
                }
            </View>
        </View>
    )
}

export default ImagePickerView
import { Dimensions, StyleSheet } from 'react-native'


const styles = StyleSheet.create({
    ImagePickerSetup: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imagePreview: {
        width: Dimensions.get('window').width - 100,
        height: 300,
        borderRadius: 10,
        padding: 20,
        borderWidth: 3, alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#fff'
    },
    image: {
        width: Dimensions.get('window').width - 100,
        height: 300,
    },
    btnSetup: {
        width: 130,
        borderRadius: 10,
        backgroundColor: 'blue',
        padding: 8,
        alignItems: 'center'
    }
})
export default styles
import React from 'react'
import { Dimensions, StyleSheet } from 'react-native'


const styles = StyleSheet.create({
    PlaceViewConatiner: {
        flexDirection: 'row',
        width: Dimensions.get('screen').width - 20,
        backgroundColor: '#f9f9f9',
        borderRadius: 10,
        margin: 5
    },
    imageConatiner: {
        flex: 0.4,
    },
    image: {
        width: '90%',
        height: 100,
        borderRadius: 10,
        margin: 10
    },
    infoConatiner: {
        flex: 0.6,
        padding: 5,
    },
    name: {
        fontSize: 22,
        fontWeight: 'bold'
    },
    city: {
        fontSize: 16,
        color: 'grey'
    },
    state: {
        fontSize: 16
    },
    description: {
        fontSize: 14
    }
})

export default styles
import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import styles from './style'

const PlaceView = ({ name, city, state, description, image, onPress }) => {
    return (
        <TouchableOpacity 
        style={styles.PlaceViewConatiner}
        onPress={onPress}>
            <View style={styles.imageConatiner}>
                <Image
                    style={styles.image}
                    source={{ uri: image }}
                />
            </View>

            <View style={styles.infoConatiner}>
                <Text style={styles.name}>{name}</Text>
                <Text style={styles.city}>{city} ,<Text style={styles.state}>{state}</Text></Text>

                <Text style={styles.description}>{description}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default PlaceView